# Locations ✖ Ktor
Implementing a subset of [Locations service spec](https://github.com/strohel/locations-rs/blob/master/api-spec.md)
using asynchronous Ktor framework.

## Alternatives

Multiple implementations of this service exist in different frameworks, languages for comparison.

- [locations-kt-http4k](https://gitlab.com/gooutopensource/locations-kt-http4k) in Kotlin http4k; complete,
- [locations-rs](https://github.com/strohel/locations-rs) in Rust Actix; contains API spec and additional tools and data for testing, benchmarking.

## Running

The service has runtime dependency on Elasticsearch instance with indices as explained in the
[`elasticsearch` directory of the locations-rs repository](https://github.com/strohel/locations-rs/tree/master/elasticsearch).

### Locally
- Make sure you have `GOOUT_ELASTIC_HOST` and `GOOUT_ELASTIC_PORT` env vars set.
- `./gradlew build --continuous` to build the app and keep watching for source changes
- `./gradlew run` to launch the app. It will reload the classes when they're changed by the build process

### In Docker
- `./gradlew shadowJar` to build the fat jar
- `docker build -t locations-ktor .` to build the docker image
- `docker run --rm -p 8080:8080 -e GOOUT_ELASTIC_HOST -e GOOUT_ELASTIC_PORT locations-ktor` to run it

## Notes from implementation by [@**goodhoko**](https://github.com/goodhoko)
- install ktor idea plugin
- project generation nice and straightforward with the plugin
- to have autoreaload you need to
  - configure the application.conf to scan for the class files
  - add a `--continuous` flag to the build task in idea run configurations
  - run both the `run` and `build` tasks simultaneously
- after DAY#1 this felt like a heaven compared to vertx
- support for lambdas and all the kotlin features makes the development very very pleasurable
- parsing enums from lowercase requires a manual workaround (https://github.com/ktorio/ktor/issues/399)
- project structure taken from the [RealWorld example app](https://github.com/dragneelfps/realworld-kotlin-ktor)
- Locations does not handle missing/optional query parameters well https://stackoverflow.com/questions/60632541/how-to-serve-http-400-when-missing-location-parameters
