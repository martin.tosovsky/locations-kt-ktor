package com.goout.controller

import com.goout.config.RequiredParameterIsNullException
import com.goout.service.CityService
import io.ktor.application.call
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.response.respond
import io.ktor.routing.Routing


fun Routing.cityController(cityService: CityService) {
    get<CityQuery>{
        if (it.id == null || it.language == null) {
            throw RequiredParameterIsNullException("id or language")
        }
        call.respond(cityService.getCity(it.id, it.language))
    }
}


@Location("/city/v1/get")
data class CityQuery(
        // unfortunately Locations currently handle required params with 404 instead of 400 https://github.com/ktorio/ktor/issues/1711
        // we workaround this by making these nullable with a default value
        val id: Long? = null,
        val language: Language? = null
)


data class CityResponse (
        val id: Long,
        val isFeatured: Boolean,
        val countryIso: String,
        val name: String,
        val regionName: String
)


enum class Language {
    CS,
    DE,
    EN,
    PL,
    SK
}
