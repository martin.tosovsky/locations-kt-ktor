package com.goout.config

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.Logger


val PipelineContext<*, ApplicationCall>.log: Logger
        get() = call.application.environment.log
