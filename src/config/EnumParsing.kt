package com.goout.config

import com.goout.controller.Language
import io.ktor.features.DataConversion


// for now a custom decoder must be provided for parsing enums from lower-case values https://github.com/ktorio/ktor/issues/399
fun DataConversion.Configuration.parseLowerCaseLanguage() {
    convert(Language::class) {
        encode {
            if (it == null)
                emptyList()
            else
                listOf((it as Language).name.toLowerCase())
        }
        decode { values, _ ->
            Language.values().first { it.name.toLowerCase() in values }
        }
    }
}
