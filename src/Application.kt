package com.goout

import com.goout.config.api
import com.goout.config.handleErrors
import com.goout.config.parseLowerCaseLanguage
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DataConversion
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.locations.Locations
import io.ktor.routing.routing
import org.slf4j.event.Level


fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)


@kotlin.jvm.JvmOverloads
fun Application.module() {
    // enable typed API params
    install(Locations)
    // log all requests
    install(CallLogging) { level = Level.INFO }
    // parse Kotlin Enums from lowercase string
    install(DataConversion) {
        parseLowerCaseLanguage()
    }
    // enable JSON serialization
    install(ContentNegotiation) {
        gson {}
    }
    // register API endpoints
    routing {
        api()
    }
    // register error handlers
    install(StatusPages) {
        handleErrors()
    }
}
